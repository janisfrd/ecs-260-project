import unittest
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
import time
import pyautogui
import random
import string



class PythonOrgSearch(unittest.TestCase):

    def is_text_present(self, text):
        return str(text) in self.driver.page_source

    def setUp(self):
        self.driver = webdriver.Remote(
   command_executor='http://127.0.0.1:4444/wd/hub',
   desired_capabilities=DesiredCapabilities.FIREFOX)
        
    def test_search_in_python_org(self):
        driver = self.driver
        driver.get("http://dave-and-mike.github.io/game-off-2012/")
        #driver.ExecuteScript("window.open('google.com','blank');")

        driver.maximize_window()  

        t=0
        time.sleep(5.0)
        pyautogui.press('enter')
        seq = [ 'down',  'up', 'right', 'c']
        while t!= 20:
            size = random.randint(1, 4)
            chosen = random.sample(seq,size);
            pyautogui.press(chosen)
            time.sleep(0.01)
            t=t+1

        elem = driver.find_element_by_id("gameWindow")
        elem.send_keys(Keys.COMMAND+ 't')
        #driver.find_element_by_tag_name('body').send_keys(Keys.COMMAND + 't') 
        time.sleep(2.0)
        pyautogui.hotkey('ctrl', 'tab') 
        time.sleep(2.0)
        while t!= 0:
            size = random.randint(1, 4)
            chosen = random.sample(seq,size);
            pyautogui.press(chosen)
            time.sleep(0.1)
            t=t-1

        time.sleep(5.0)


            # print "looped"
            # if "Aw, Snap" in driver.page_source:
            #     time.sleep(5.0)
            #     break
            # if "Aw, Snap" in driver.page_source:
            #     time.sleep(2.0)
            #     driver.get("http://szhu.github.io/3030/")
            #     for i in range(0,10):
            #         pyautogui.press('down')
        # #assert "Game over" not in driver.page_source


    def tearDown(self):
       self.driver.close()

if __name__ == "__main__":
    unittest.main()