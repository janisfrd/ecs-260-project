import time
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
browser = webdriver.Remote(
   command_executor='http://127.0.0.1:4444/wd/hub',
   desired_capabilities=DesiredCapabilities.FIREFOX)
home_page = "http://www.adamatomic.com/mode/"


def setUp(self):
        self.driver = webdriver.Remote(
   command_executor='http://127.0.0.1:4444/wd/hub',
   desired_capabilities=DesiredCapabilities.FIREFOX)

def check_page_broken_links(self, url):
# Sample usage:
#     check_page_broken_links(self,"http://www.adamatomic.com/mode/") 
#         will return empty list if all links in the page work fine
#         else it will return list of all the broken links 
#                                    (either link text, or link href)
#   Will check for -  i) "Page Not found" error
#                     ii) Redirects
    try:
        driver = self
        failed = []
        driver.get(url)
        driver.maximize_window()
        number_of_links = len(driver.find_elements_by_tag_name('a'))
        time.sleep(2)
        for i in range(number_of_links):
            # Save current browser window handle
            initial_window = driver.current_window_handle 

            link = driver.find_elements_by_tag_name('a')[i]
            link_address = link.get_attribute("href")
            link_name = link.text
            print "link checked: ",i+1," - ",link_name

            if ((link_address is not None) 
                and ("google" not in link_address) 
                and ("mailto" not in link_address)
                and ("#" not in link_address) 
                is True):
                  link.click() # link clicked
                  open_windows = driver.window_handles

                  # # Navigate to the browser window where 
                  # #               latest page was opened
                  driver.switch_to_window(open_windows[-1])
                  ## print "current_window_handle:"
                  ## print self.current_window_handle
                  time.sleep(1)
                  print "defined: ",link_address
                  print "current: ", driver.current_url


                  if (link_address[-1] == "#" 
                       and driver.current_url in 
                        [link_address, link_address[:-1],
                                      link_address[:-2],link_address+'/']):
                          # A "#" at the end means user 
                          # will stay on the same page.(Valid scenario) 
                          pass
                  elif (str("not found") in driver.title) or (str("404") in driver.page_source):
                    print "Unfortunately, this link is broken!"
                    if link_name:
                      failed.append(link_name)
                    else:
                      failed.append(link_address)
                  

                  if len(driver.window_handles) > 1:  
                          # close newly opened window
                          driver.close()

                  # Switch to main browser window
                  driver.switch_to_window(open_windows[0])
            driver.get(url)
    except Exception, e: 
           return ['Exception occurred while checking',e]
    time.sleep(3)
    driver.close()
    return failed

# call defined function to check broken links in adamatomic game page
print check_page_broken_links(browser,"http://www.adamatomic.com/mode/")