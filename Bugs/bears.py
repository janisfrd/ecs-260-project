import unittest
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
import time
import pyautogui
import random
import string



class PythonOrgSearch(unittest.TestCase):

    def is_text_present(self, text):
        return str(text) in self.driver.page_source

    def setUp(self):
        self.driver = webdriver.Remote(
   command_executor='http://127.0.0.1:4444/wd/hub',
   desired_capabilities=DesiredCapabilities.FIREFOX)
        
    def test_search_in_python_org(self):
        driver = self.driver
        driver.get("http://szhu.github.io/3030/")
        driver.maximize_window()    
        t=0
        pyautogui.moveTo(257, 250,1, pyautogui.easeInQuad)
        x=257
        y=279
        time.sleep(1.0)
        for i in range(0,10):
            pyautogui.press('down')
        time.sleep(1.0)
        pyautogui.moveTo(x, y, 1, pyautogui.easeInQuad)
        nos=['324']
        xvals=['235']
        for i in range(324, 328):
            nos.append(i)
        for i in range(475, 492):
            nos.append(i)
        for i in range(526, 586):
            nos.append(i)
        for i in range(500, 521):
            nos.append(i)
        while t!= 100:
            pyautogui.mouseDown(button='right')
            if x>885:
                x=257
            for i in range(x+280, x+300):
                xvals.append(i)
            x = random.choice(xvals)

            y = random.choice(nos)

            pyautogui.moveTo(x, y, 1, pyautogui.easeInQuad)
            
            

            pyautogui.mouseUp(button='right', x=x, y=y)


            if self.is_text_present("Something went wrong") or driver.current_url != "http://szhu.github.io/3030/" :
                driver.get("http://szhu.github.io/3030/")
                time.sleep(1.0)
                pyautogui.click()
                pyautogui.moveTo(257, 279,0.2, pyautogui.easeInQuad)
                x=257
                x=279
                time.sleep(1.0)
                for i in range(0,10):
                    pyautogui.press('down')
            else:
                time.sleep(1.0)

            t=t+1



    def tearDown(self):
       self.driver.close()

if __name__ == "__main__":
    unittest.main()