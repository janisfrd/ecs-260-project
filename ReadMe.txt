- Open a terminal, navigate to directory of project and run "python get-pip.py" in the terminal
- run "sudo pip install image" in the terminal
- run "sudo pip install pyautogui" in the terminal

Setup selenium web driver for Firefox by typing: "sudo pip install Selenium"

Follow steps in http://selenium-python.readthedocs.org/installation.html to download Selenium server and run Server in seperate terminal using command:
- "java -jar selenium-server-standalone-2.x.x.jar", where the x's indicate your version.

Open another terminal for your client.

Run scripts:

So now we come to the main event. We have different monkeys/scripts that were run on games, they randomly simulate events but to reproduce errors, 
we have added the files(Bugs folder) with hardcoded values as well (not for all) because we do not want you to run a script 
and wait for the entire execution of the Monkeys(Monkeys folder) to see that one input that causes the error. 
All these scripts are run in the terminal for your client and output can be seen on terminal client, and the execution can be seen in the console
where the server is as well.
Bugs: To run each file use : python file_name.py
1) particle_clicker.py
This will open the url "http://particle-clicker.web.cern.ch/particle-clicker/" and simulate 10 random clicks, the error is that it should only allow clicks
within the bounded circle or the particle accelerator but even in some areas outside, it registers as a valid click and increments the "Data" variable.
We have not included a separate reproduciblity script for this because our monkey here just simulates 10 clicks on one run and due to the range of the
co-ordinates from which we are randomly selecting values, there is bound to be atleast one buggy click in a maximum of 3 runs.

2) links_with_windows_server.py
This will open the url "http://www.adamatomic.com/mode/" and click on both links one by one, it visits both pages. Since the "View Source" link leads
to a 404 page, it adds it to an array called failed, when you look at the client terminal, it shows that the link is broken while visiting it and at the
end of the script, the link is also returned in the 'failed' array. To test this Link Monkey on any other website, the url just needs to be changed at the
bottom and the top of the script and it will look for broken link on any website, not just this one.

3)clone,py
Here, "http://dave-and-mike.github.io/game-off-2012/" is opened where an alien is moving either up, down, left or right to move through doors. In this 
game, we found that once we switch tabs and come back, the viewport doesn't render properly.

4)bears.py
http://szhu.github.io/3030/ leads to a game developed over a software bug where if you hover over a link with extension %%30%30, it redirects to "Aw, Snap". So a maze
with bears and trees has been created, where the trees are links with the bug and bears are not. We are supposed to just move along the bears sequence till the exit 
without touching the trees. Unfortunately, there's a simpler way around it by just reight clicking and moving through the menu over the trees as long as you end in a 
safe place i.e bear or outside the maze.

Monkeys(full scripts): Run as python file_name.py
AggressiveMonkey: Keyboard and mouse events for all the keys on the keyboard randomly without time delays
CrazyMonkey: Keyboard and mouse events for all the keys randomly with variable time delays
MonkeyPlayer: Keyboard and mouse events which are only to play game (eg. Arrow keys) with variable time delays
LinkMonkey:Links on page selected at random and tested

NOTE: TO TERMINATE PYAUTOGUI IN THE MIDDLE, MOVE MOUSE TO TOP LEFT CORNER OF THE SCREEN OR DO 'ctrl' + 'C' ON THE CONSOLE.
LINK TO DEMO: https://www.dropbox.com/s/t7ca4otfie1gjdc/Demo.mov?dl=0
